import axios from "axios";
import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';

function AdminPage() {
    const navigate = useNavigate();

    const navigateToUsers = () => {
        if(localStorage.getItem('role') === "ADMIN")
                navigate("/users");
        else
            alert("You are not authorized to access this page");
    }

    const navigateToDevices = () => {
        if(localStorage.getItem('role') === "ADMIN")
           navigate("/alldevices");
        else
            alert("You are not authorized to access this page");
    }

    useEffect(() => {
        // Add any necessary side effects or data fetching here
    }, []);

    return (
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
            <h1 style={{ marginBottom: '20px' }}>Admin Page</h1>
            <p style={{ fontSize: '18px', marginBottom: '40px', textAlign: 'center' }}>Restricted area! Only users with the admin role are authorized.</p>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '20px' }}>
                <div style={{ marginRight: '20px' }}>
                    <p style={{ fontSize: '16px', marginBottom: '10px' }}>Click here to manage the users in the system</p>
                    <button onClick={navigateToUsers} style={{ padding: '10px 20px', fontSize: '16px' }}>Manage Users</button>
                </div>
                <div>
                    <p style={{ fontSize: '16px', marginBottom: '10px' }}>Click here to manage the devices in the system</p>
                    <button onClick={navigateToDevices} style={{ padding: '10px 20px', fontSize: '16px' }}>Manage Devices</button>
                </div>
            </div>
        </div>
    );
}

export default AdminPage;
