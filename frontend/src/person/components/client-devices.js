import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';

function ClientDevices()
{
  const navigate = useNavigate();  
  const {id} = useParams();
  const [devices, setdevices] = useState([])
    useEffect(() => { 
        loaddevices();
    });
    
    const loaddevices = async() =>
        {
        const result = await axios.get(`http://localhost:8081/spring-demo/client/userdevice/${id}`);
        setdevices(result.data);

    }
    return (
        <div className="container">
        <div className="py-4">
          <table className="table border shadow">
            <thead>
                        <tr>
                <th scope="col">Description</th>
                <th scope="col">Address</th>
                <th scope="col">Max Consumption</th>
              </tr>
            </thead>
            <tbody>
              {devices.map((user, index) => (
                <tr>
                  <td>{user.description}</td>
                  <td>{user.address}</td>
                  <td>{user.max_consumption}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
}
export default ClientDevices;