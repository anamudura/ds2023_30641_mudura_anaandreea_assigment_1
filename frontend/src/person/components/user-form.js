
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';


const UserForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState('');
  
  const navigate = useNavigate();


  async function save(event) {
    event.preventDefault();
    try {
      await axios.post("http://localhost:8080/spring-demo/admin/newuser", {
      email: email,
      role: role,
      name: name,
      password: password,
      });
      alert("Employee Registation Successfully");
      if(localStorage.getItem('role') === "ADMIN")
        navigate('/users');
      else
        alert("You are not authorized to access this page");

    } catch (err) {
      alert(err);
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
      <label>
        Name:
        <input type="text" value={name} onChange={(event) => setName(event.target.value)} />
      </label>
      <br />
      <label>
        Email:
        <input type="email" value={email} onChange={(event) => setEmail(event.target.value)} />
      </label>
      <br />
      <label>
        Password:
        <input type="password" value={password} onChange={(event) => setPassword(event.target.value)} />
      </label>
      <br />
      <label>
        Role:
        <input type="role" value={role} onChange={(event) => setRole(event.target.value)} />
      </label>
      <br />
      <button type="submit" class="btn btn-primary mt-4" onClick={save} >Register</button>
        </div>
        </div>
        </div>
  );
};

export default UserForm;
