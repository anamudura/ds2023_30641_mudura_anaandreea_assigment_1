import axios from "axios";
import React, { useEffect } from "react";
import { useNavigate, useParams } from 'react-router-dom';

function ClientPage() {
    const {id}= useParams();
    const navigate = useNavigate();


    const navigateToDevices = () => {
        if(localStorage.getItem('role') === "CLIENT")
            navigate(`/usersdevice/${id}`);
        else
            alert("You are not authorized to access this page");
    }

    useEffect(() => {
        // Add any necessary side effects or data fetching here
    }, []);

    return (
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
            <h1 style={{ marginBottom: '20px' }}>Client Page</h1>
            <p style={{ fontSize: '18px', marginBottom: '40px', textAlign: 'center' }}>Welcome Client.</p>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '20px' }}>
                    <p style={{ fontSize: '16px', marginBottom: '10px' }}>Click here to see all your devices</p>
                    <button onClick={navigateToDevices} style={{ padding: '10px 20px', fontSize: '16px' }}>See your devices Devices</button>
                </div>
            </div>
    );
}

export default ClientPage;
