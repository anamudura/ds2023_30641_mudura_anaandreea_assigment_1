import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';

function AllDevices()
{
  const navigate = useNavigate();  
  const [devices, setdevices] = useState([])
    useEffect(() => { 
        loaddevices();
    });
    
    const loaddevices = async() =>
        {
        const result = await axios.get("http://localhost:8081/spring-demo/admin/alldevices");
        setdevices(result.data);

    }
    const deletedevice = async (id) => {
        await axios.post(`http://localhost:8081/spring-demo/admin/deletedevice/${id}`);
        loaddevices();
      };
      const RegisterDevice = () => {
        // 👇️ navigate to /contacts
        if(localStorage.getItem('role') === "ADMIN")
          navigate('/newdevice');
        else
          alert("You are not authorized to access this page");
      };
    return (
        <div className="container">
        <div className="py-4">
          <table className="table border shadow">
            <thead>
                        <tr>
                <th scope="col">Description</th>
                <th scope="col">Address</th>
                <th scope="col">Max Consumption</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {devices.map((user, index) => (
                <tr>
                  <td>{user.description}</td>
                  <td>{user.address}</td>
                  <td>{user.max_consumption}</td>
                  <td>
                    <Link
                      className="btn btn-outline-primary mx-2"
                      to={`/updatedevice/${user.id}`}
                    >
                      Edit
                    </Link>
                    <button
                      className="btn btn-danger mx-2"
                      onClick={() => deletedevice(user.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <button onClick={RegisterDevice}>Add New Device</button>
        </div>
      </div>
    );
}
export default AllDevices;