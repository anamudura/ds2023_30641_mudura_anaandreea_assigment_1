
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';


const DeviceForm = () => {
  const [address, setaddress] = useState('');
  const [description, setdescription] = useState('');
  const [max_consumption, setmax_consumption] = useState('');

  const [users, setusers] = useState([])
  useEffect(() => { 
      loadusers();
  });
  

  const loadusers = async() =>
        {
        const result = await axios.get("http://localhost:8080/spring-demo/admin/allusers");
        setusers(result.data);

    }
  
  const navigate = useNavigate();


  async function save(id) {
    try {
      await axios.post(`http://localhost:8081/spring-demo/admin/newdevice/${id}`, {
      description: description,
      address: address,
      max_consumption: max_consumption,
      });
      alert("Employee Registation Successfully");
      if(localStorage.getItem('role') === "ADMIN")
        navigate('/alldevices');
      else
        alert("You are not authorized to access this page");

    } catch (err) {
      alert(err);
    }
  }

  return (
    <><div classaddress="container">
          <div classaddress="row">
              <div classaddress="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                  <label>
                      address:
                      <input type="text" value={address} onChange={(event) => setaddress(event.target.value)} />
                  </label>
                  <br />
                  <label>
                      description:
                      <input type="description" value={description} onChange={(event) => setdescription(event.target.value)} />
                  </label>
                  <br />
                  <label>
                      max_consumption:
                      <input type="max_consumption" value={max_consumption} onChange={(event) => setmax_consumption(event.target.value)} />
                  </label>
                  <br />
                  <br />
                  
              </div>
          </div>
      </div><div className="container">
              <div className="py-4">
                  <table className="table border shadow">
                      <thead>
                          <tr>
                              <th scope="col">Name</th>
                              <th scope="col">Email</th>
                              <th scope="col">Role</th>
                              <th scope="col">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          {users.map((user, index) => (
                              <tr>
                                  <th scope="row" key={index}>
                                      {index + 1}
                                  </th>
                                  <td>{user.name}</td>
                                  <td>{user.email}</td>
                                  <td>{user.role}</td>
                                  <td>
                                      <button
                                          className="btn btn-danger mx-2"
                                          onClick={() => save(user.id)}
                                      >
                                          Select this user
                                      </button>
                                  </td>
                              </tr>
                          ))}
                      </tbody>
                  </table>
              </div>
          </div></>
  );
};

export default DeviceForm;
