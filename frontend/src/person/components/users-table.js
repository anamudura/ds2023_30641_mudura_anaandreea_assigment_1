import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';

function AllUsers()
{
  const navigate = useNavigate();  
  const [users, setusers] = useState([])
    useEffect(() => { 
        loadusers();
    });
    
    const loadusers = async() =>
        {
        const result = await axios.get("http://localhost:8080/spring-demo/admin/allusers");
        setusers(result.data);

    }
    const deleteUser = async (id) => {
        await axios.post(`http://localhost:8080/spring-demo/admin/deleteuser/${id}`);
        loadusers();
      };
      const RegisterUser = () => {
        // 👇️ navigate to /contacts
        if(localStorage.getItem('role') === "ADMIN")
             navigate('/newuser');
        else
          alert("You are not authorized to access this page");
      };
    return (
        <div className="container">
        <div className="py-4">
          <table className="table border shadow">
            <thead>
                        <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user, index) => (
                <tr>
                  <th scope="row" key={index}>
                    {index + 1}
                  </th>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.role}</td>
                  <td>
                    <Link
                      className="btn btn-outline-primary mx-2"
                      to={`/edituser/${user.id}`}
                    >
                      Edit
                    </Link>
                    <button
                      className="btn btn-danger mx-2"
                      onClick={() => deleteUser(user.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <button onClick={RegisterUser}>Add New User</button>
        </div>
      </div>
    );
}
export default AllUsers;