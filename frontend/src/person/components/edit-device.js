import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";

export default function EditDevice() {
  let navigate = useNavigate();

  const { id } = useParams();

  const [device, setdevice] = useState({
    address: "",
    description: "",
    max_consumption: "",
  });

  const { address, description, max_consumption} = device;

  const onInputChange = (e) => {
    setdevice({ ...device, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loaddevice();
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.post(`http://localhost:8081/spring-demo/admin/updatedevice/${id}`, device);
    if(localStorage.getItem('role') === "ADMIN")
     navigate("/alldevices");
    else
      alert("You are not authorized to access this page");
  };

  const loaddevice = async () => {
    const result = await axios.get(`http://localhost:8081/spring-demo/admin/device/${id}`);
    setdevice(result.data);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
          <h2 className="text-center m-4">Edit Device</h2>

          <form onSubmit={(e) => onSubmit(e)}>
            <div className="mb-3">
              <label htmlFor="Address" className="form-label">
                Address
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter new address"
                name="address"
                value={device.address}
                onChange={(e) => onInputChange(e)}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Description" className="form-label">
                Description
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter new description"
                name="description"
                value={device.description}
                onChange={(e) => onInputChange(e)}
              />
            </div>
                      <div className="mb-3">
              <label htmlFor="MaxCons" className="form-label">
                Maximum Consumption
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter new consumption"
                name="max_consumption"
                value={device.max_consumption}
                onChange={(e) => onInputChange(e)}
              />
            </div>
            <button type="submit" className="btn btn-outline-primary">
              Submit
            </button>
            <Link className="btn btn-outline-danger mx-2" to="/alldevices">
              Cancel
                      </Link>
          </form>
        </div>
      </div>
    </div>
  );
}