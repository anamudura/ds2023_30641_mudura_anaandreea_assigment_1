import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import NavigationBar from './navigation-bar';
import Login from './home/Login';
import PersonContainer from './person/person-container';
import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import AdminPage from './person/components/admin-page';
import AllUsers from './person/components/users-table';
import EditUser from './person/components/edit-user';
import UserForm from './person/components/user-form';
import AllDevices from './person/components/devices-table';
import EditDevice from './person/components/edit-device';
import DeviceForm from './person/components/device-form';
import ClientPage from './person/components/client-page';
import ClientDevices from './person/components/client-devices';

/*
    Namings: https://reactjs.org/docs/jsx-in-depth.html#html-tags-vs.-react-components
    Should I use hooks?: https://reactjs.org/docs/hooks-faq.html#should-i-use-hooks-classes-or-a-mix-of-both
*/
function App() {
    return (
        <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Routes>

                    <Route path="/" element={<Login />} />
                    <Route path="/admin" element={<AdminPage />} />
                    <Route path="/client/:id" element={<ClientPage />} />
                    <Route path="/usersdevice/:id" element={<ClientDevices />} />
                    <Route path="/users" element={<AllUsers />} />
                    <Route path="/edituser/:id" element={<EditUser />} />
                    <Route path="/updatedevice/:id" element={<EditDevice />} />
                    <Route path="/newuser" element={<UserForm />} />
                    <Route path="/alldevices" element={<AllDevices />} />
                    <Route path="/newdevice" element={<DeviceForm />} />


                        <Route render={() => <ErrorPage />} />
                    </Routes>
                </div>
            </Router>
        </div>
    );
}

export default App;
