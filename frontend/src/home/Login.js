import React, {  useState } from "react";
import { useNavigate } from 'react-router-dom';
import axios from "axios";
import { Routes, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';


function Login() {
   
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const [user, setUser] = useState();

    const navigateToRegister = () => {
      navigate('/register');
    };
  
    const navigateHome = () => {
      navigate('/');
    };
    async function login(event) {
        event.preventDefault();
        try {
          await axios.post("http://localhost:8080/spring-demo/login", {
            email: email,
            password: password,
            }).then((res) => 
            {
              console.log(res.data);
              const currentUser = res.data;
              setUser(res.data);
             
              if(currentUser.role === "ADMIN")
              {
                localStorage.setItem('role', currentUser.role);
                if(localStorage.getItem('role') === "ADMIN")
                      navigate('/admin');
                else
                      alert("You are not authorized to access this page");    
              }
              else
              if(currentUser.role === "CLIENT")
              {
                localStorage.setItem('role', currentUser.role);
                if(localStorage.getItem('role') === "CLIENT")
                  navigate(`/client/${currentUser.id}`);
                else
                  alert("You are not authorized to access this page");
              }
          }, fail => {
           console.error(fail); // Error!
  });
        }

 
         catch (err) {
          alert(err);
        }
      
      }
   
   
   
   
   
   
    return (
       <div>
            <div class="container">
            <div class="row">
                <h2>WELCOME TO THE ENERGY MANAGMENT SYSTEM</h2>
             <hr/>
             </div>

             <div class="row">
             <div class="col-sm-6">
 
            <form>
        <div class="form-group">
          <label>Email</label>
          <input type="email"  class="form-control" id="email" placeholder="Enter username"
          
          value={email}
          onChange={(event) => {
            setEmail(event.target.value);
          }}
          
          />

        </div>

        <div class="form-group">
            <label>password</label>
            <input type="password"  class="form-control" id="password" placeholder="Enter Password"
            
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
            
            />
          </div>
                            <button type="submit" class="btn btn-primary" onClick={login} >Login</button>
                            <button type="submit" class="btn btn-primary" onClick={navigateToRegister} >Register</button>
                            
              </form>

            </div>
            </div>
            </div>

     </div>
    );
  }
  
  export default Login;