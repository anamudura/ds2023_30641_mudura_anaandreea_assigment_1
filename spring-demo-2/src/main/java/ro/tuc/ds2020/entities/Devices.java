package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Devices {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String address;
    private String description;
    private String max_consumption;

    @ManyToOne
    @JsonBackReference
    private AppUser user;

    public Devices(String address, String description, String max_consumption) {
        this.address = address;
        this.description = description;
        this.max_consumption = max_consumption;
    }
}
