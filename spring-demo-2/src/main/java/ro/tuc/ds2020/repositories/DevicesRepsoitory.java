package ro.tuc.ds2020.repositories;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Devices;

import java.util.List;
@Repository
@Transactional
public interface DevicesRepsoitory extends JpaRepository<Devices, Long>{
    //get list of devices based on user id
    @Query("select d from Devices d where d.user.id = ?1")
    List<Devices> findByUserId(Long id);
}
