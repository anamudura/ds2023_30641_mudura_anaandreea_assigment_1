package ro.tuc.ds2020.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Devices;
import ro.tuc.ds2020.repositories.AppUsersRepository;
import ro.tuc.ds2020.repositories.DevicesRepsoitory;

import java.util.Optional;

@AllArgsConstructor
@RestController
@CrossOrigin("https://localhost:3000")
@RequestMapping("/admin")
public class AdminController {
    private final AppUsersRepository appUserRepository;
    private final DevicesRepsoitory devicesRepsoitory;

    //CREATE DEVICE
    @PostMapping("/newdevice/{id}")
    public ResponseEntity<?> createNewDevice(@PathVariable("id")Long id, @RequestBody Devices devices)
    {
        if(devices.getAddress() == null)
        {
            System.out.println("Invalid data input");
            return ResponseEntity.internalServerError().body("Invalid input for creating new device");
        }
        else {

            devices.setUser(appUserRepository.findById(id).orElse(null));
            devicesRepsoitory.save(devices);
            return ResponseEntity.ok("New device added");
        }
    }

    //DELETE DEVICE
    @PostMapping("/deletedevice/{id}")
    public ResponseEntity<?> deleteDevice(@PathVariable("id") Long id)
    {
        Optional<Devices> devices = devicesRepsoitory.findById(id);
        if(devices.isEmpty())
        {
            System.out.println("The device was not found in the database");
            return ResponseEntity.internalServerError().body("Device is empty");
        }
        else {
            devicesRepsoitory.deleteById(id);
            return ResponseEntity.ok("Device deleted");
        }
    }

    //UPDATE DEVICE
    @PostMapping("/updatedevice/{id}")
    public ResponseEntity<?> updateDevice(@PathVariable("id") Long id, @RequestBody Devices devices)
    {
        Optional<Devices> devicesOptional = devicesRepsoitory.findById(id);
        if(devicesOptional.isEmpty())
        {
            System.out.println("The device was not found in the database");
            return ResponseEntity.internalServerError().body("Device is empty");
        }
        else {
            Devices device = devicesOptional.get();
            device.setAddress(devices.getAddress());
            device.setDescription(devices.getDescription());
            device.setMax_consumption(devices.getMax_consumption());
            devicesRepsoitory.save(device);
            return ResponseEntity.ok("Device updated");
        }
    }

    //READ ALL DEVICES
    @GetMapping("/alldevices")
    public ResponseEntity<?>getAllDevices()
    {
        return ResponseEntity.ok(devicesRepsoitory.findAll());
    }

    //GET DEVICE OF A CERTAIN USER BY ID
    @GetMapping("/userdevice/{id}")
    public ResponseEntity<?> getDevicesByUserId(@PathVariable("id") Long id)
    {
        return ResponseEntity.ok(devicesRepsoitory.findByUserId(id));
    }
    //GET DEVICE BY ID
    @GetMapping("/device/{id}")
    public ResponseEntity<?> getDeviceById(@PathVariable("id") Long id)
    {
        Optional<Devices> devices = devicesRepsoitory.findById(id);
        if(devices.isEmpty())
        {
            System.out.println("The device was not found in the database");
            return ResponseEntity.internalServerError().body("Device is empty");
        }
        else {
            return ResponseEntity.ok(devices);
        }
    }

}
