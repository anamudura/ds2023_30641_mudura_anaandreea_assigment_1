package ro.tuc.ds2020.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Devices;
import ro.tuc.ds2020.repositories.AppUsersRepository;
import ro.tuc.ds2020.repositories.DevicesRepsoitory;

import java.util.Optional;

@AllArgsConstructor
@RestController
@CrossOrigin("https://localhost:3000")
@RequestMapping("/client")
public class ClientController {

    private final AppUsersRepository appUserRepository;
    private final DevicesRepsoitory devicesRepsoitory;
    @GetMapping("/userdevice/{id}")
    public ResponseEntity<?> getDevicesByUserId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(devicesRepsoitory.findByUserId(id));
    }

}
