package ro.tuc.ds2020.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.UserLoginDto;
import ro.tuc.ds2020.entities.AppUser;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.repositories.AppUserRepository;
import ro.tuc.ds2020.utils.AppException;

@AllArgsConstructor
@Getter
@Setter
@Service
public class AppUserService {
    public final AppUserRepository appUserRepository;
    public void saveUser(AppUser appUser)
    {
        String name = appUser.getName();
        String email = appUser.getEmail();
        String password = appUser.getPassword();
        Role role = appUser.getRole();
        AppUser appUser1 = new AppUser(name,email,password,role);
        appUserRepository.save(appUser1);
    }
    //update user
    public void updateUser(AppUser appUser)
    {
        String name = appUser.getName();
        String email = appUser.getEmail();
        String password = appUser.getPassword();
        Role role = appUser.getRole();
        //set the new values
        appUser.setName(name);
        appUser.setEmail(email);
        appUser.setPassword(password);
        appUser.setRole(role);
        appUserRepository.save(appUser);
    }

    public UserLoginDto findByLogin(String email) {
        AppUser user = appUserRepository.findByEmail(email);
        return new UserLoginDto(user.getEmail(), user.getPassword());
    }
}
