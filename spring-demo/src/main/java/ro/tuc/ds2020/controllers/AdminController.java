package ro.tuc.ds2020.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.AppUser;
import ro.tuc.ds2020.repositories.AppUserRepository;
import ro.tuc.ds2020.services.AppUserService;

@AllArgsConstructor
@RestController
@CrossOrigin("https://localhost:3003")
@RequestMapping("/admin")
public class AdminController {

    private final AppUserRepository appUserRepository;
    private final AppUserService appUserService;

    //CREATE USER
    @PostMapping("/newuser")
    public ResponseEntity<?> createNewUser(@RequestBody AppUser appUser)
    {
        if(appUser.getEmail() == null)
        {
            System.out.println("Invalid data input");
            return ResponseEntity.internalServerError().body("Invalid input for creating new user");
        }
        else {
            appUserService.saveUser(appUser);
            return ResponseEntity.ok(appUser);
        }
    }
    //DELETE USER
    @PostMapping("/deleteuser/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id)
    {
        AppUser appUser = appUserRepository.findById(id).orElse(null);
        if(appUser == null)
        {
            System.out.println("The user was not found in the database");
            return ResponseEntity.internalServerError().body("User is empty");
        }
        else {
            appUserRepository.deleteById(id);
            return ResponseEntity.ok("User deleted");
        }
    }
    //UPDATE USER
    @PostMapping("/updateuser/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody AppUser appUser)
    {
        AppUser appUserOptional = appUserRepository.findById(id).orElse(null);
        if(appUserOptional == null)
        {
            System.out.println("The user was not found in the database");
            return ResponseEntity.internalServerError().body("User is empty");
        }
        else {
            appUser.setId(appUserOptional.getId());
            appUserService.updateUser(appUser);
            return ResponseEntity.ok("User updated");
        }
    }
    //ALL USERS
    @GetMapping("/allusers")
    public ResponseEntity<?> getAllUsers()
    {
        return ResponseEntity.ok(appUserRepository.findAll());
    }

    //GET A USER BY ID
    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") Long id)
    {
        AppUser appUser = appUserRepository.findById(id).orElse(null);
        if(appUser == null)
        {
            System.out.println("The user was not found in the database");
            return ResponseEntity.internalServerError().body("User is empty");
        }
        else {
            return ResponseEntity.ok(appUser);
        }
    }

}
