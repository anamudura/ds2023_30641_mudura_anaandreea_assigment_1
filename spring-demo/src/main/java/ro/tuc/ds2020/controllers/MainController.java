package ro.tuc.ds2020.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.UserLoginDto;
import ro.tuc.ds2020.entities.AppUser;
import ro.tuc.ds2020.repositories.AppUserRepository;

@AllArgsConstructor
@RestController
@CrossOrigin("https://localhost:3003")
@RequestMapping("")
public class MainController {
    private final AppUserRepository appUserRepository;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserLoginDto userLoginDto)
    {
        AppUser appUser = appUserRepository.findByEmail(userLoginDto.getEmail());
        if(appUser == null)
        {
            System.out.println("User is not in the database");
            return ResponseEntity.internalServerError().body("Invalid user");

        }
        else
        {
            System.out.println("LOGIN SUCCESSFUL");
            return ResponseEntity.ok(appUser);
        }
    }
}
